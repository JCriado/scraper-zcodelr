<?php
	class ZCodeScraper{
		private $html;
		private $scraper;
		private $scraper_xpath;
		
		public function __construct($url){
			$this->html = file_get_contents($url);
			$this->scraper = new DOMDocument();
			$this->scraper->loadHTML($this->html);
			libxml_clear_errors(); //remove errors for yucky html
			error_reporting(E_ERROR);
			$this->scraper_xpath = new DOMXPath($this->scraper);
			
		}
		public function get_equipos(){
			//Seleccionamos los div que tengan la clase team
			$teams = $this->scraper_xpath->query('//div[@class="team"]');
			//Almacenamos los equipos en un array
			if($teams->length > 0){
		      	$equipos = array();
		      	foreach($teams as $team){
		          	array_push($equipos, $team->nodeValue);
		      	}
		  	}
			return $equipos;
		}
		public function get_fecha_partidos(){
			//Seleccionamos los td que tengan la clase Date
			$dates = $this->scraper_xpath->query('//td[@class="Date"]');
			//Almacenamos las fechas en un array
			if($dates->length > 0){
				$fechas = array();
		      	foreach($dates as $date){
		      		array_push($fechas, $date->nodeValue);
		      	}
		  	}
			return $fechas;
		}
		public function get_ml_publics(){
			//Seleccionamos todos los ML public
			$ml_publics = $this->scraper_xpath->query('//div[contains(@class,"public_tickets ppc ToML")]');
			//Almacenamos los ml en un array
			if($ml_publics->length > 0){
				$money_lines = array();
		      	foreach($ml_publics as $ml){
		      		array_push($money_lines, $ml->nodeValue);
		      	}
		  	}
			return $money_lines;
		}
		public function get_sp_publics(){
			//Seleccionamos todos los Spread public
			$sp_publics = $this->scraper_xpath->query('//div[contains(@class,"public_tickets ppc ToSpread")]');
			//Almacenamos los spread en un array
			if($sp_publics->length > 0){
				$spreads = array();
		      	foreach($sp_publics as $sp){
		      		array_push($spreads, $sp->nodeValue);
		      	}
		  	}
			return $spreads;
		}
		public function get_to_publics(){
			//Seleccionamos todos los Total public (over/under)
			$to_publics = $this->scraper_xpath->query('//div[contains(@class,"public_tickets ppc ToTotal")]');
			//Almacenamos los totals en un array
			if($to_publics->length > 0){
				$totals = array();
		      	foreach($to_publics as $to){
		      		array_push($totals, $to->nodeValue);
		      	}
		  	}
			return $totals;
		}
		public function get_ml_tickets(){
			//Seleccionamos todos los ML tickets
			$ml_tickets = $this->scraper_xpath->query('//div[contains(@class,"public_tickets tickets ToML")]');
			//Almacenamos los ml tickets en un array
			if($ml_tickets->length > 0){
				$money_lines_tickets = array();
		      	foreach($ml_tickets as $ml){
		      		array_push($money_lines_tickets, $ml->nodeValue);
		      	}
		  	}
			return $money_lines_tickets;
		}
		public function get_sp_tickets(){
			//Seleccionamos todos los Spread tickets
			$sp_tickets = $this->scraper_xpath->query('//div[contains(@class,"public_tickets tickets ToSpread")]');
			//Almacenamos los spread tickets en un array
			if($sp_tickets->length > 0){
				$spreads_tickets = array();
		      	foreach($sp_tickets as $sp){
		      		array_push($spreads_tickets, $sp->nodeValue);
		      	}
		  	}
			return $spreads_tickets;
		}
		
		public function get_to_tickets(){
			//Seleccionamos todos los Total tickets (over/under)
			$to_tickets = $this->scraper_xpath->query('//div[contains(@class,"public_tickets tickets ToTotal")]');
			//Almacenamos los totals tickets en un array
			if($to_tickets->length > 0){
				$totals_tickets = array();
		      	foreach($to_tickets as $to){
		      		array_push($totals_tickets, $to->nodeValue);
		      	}
		  	}
			return $totals_tickets;
		}
		public function crear_partidos($equipos, $fechas, $mlp, $spp, $top, $mlt, $spt, $tot){
			//Emparejamos los equipos por partidos
			$cont = 0;
			$partidos = array();
			for ($i=0; $i < sizeof($equipos); $i++) { 
				$partidos[$cont]['Visitante'] = $equipos[$i];
				$partidos[$cont]['Local'] = $equipos[$i+1];
				$partidos[$cont]['Fecha'] = $fechas[$cont];
				//Publics
				$mlp[$i] = explode(": ", $mlp[$i]);
				$mlp[$i+1] = explode(": ", $mlp[$i+1]);
				$spp[$i] = explode(": ", $spp[$i]);
				$spp[$i+1] = explode(": ", $spp[$i+1]);
				$top[$i] = explode(": ", $top[$i]);
				$top[$i+1] = explode(": ", $top[$i+1]);
				$over_vis = explode(", ", $top[$i][1]);
				$over_vis = $over_vis[0];
				$over_loc = explode(", ", $top[$i+1][1]);
				$over_loc = $over_loc[0];
				//Tickets
				$mlt[$i] = explode(": ", $mlt[$i]);
				$ml_tickets_vis = explode(" (", $mlt[$i][1]);
				$mlt[$i+1] = explode(": ", $mlt[$i+1]);
				$ml_tickets_loc = explode(" (", $mlt[$i+1][1]);
				$spt[$i] = explode(": ", $spt[$i]);
				$sp_tickets_vis = explode(" (", $spt[$i][1]);
				$spt[$i+1] = explode(": ", $spt[$i+1]);
				$sp_tickets_loc = explode(" (", $spt[$i+1][1]);
				$tot[$i] = explode(": ", $tot[$i]);
				$tot[$i+1] = explode(": ", $tot[$i+1]);
				$over_vis_tickets = explode(", ", $tot[$i][1]);
				$over_vis_tickets = $over_vis_tickets[0];
				$over_loc_tickets = explode(", ", $tot[$i+1][1]);
				$over_loc_tickets = $over_loc_tickets[0];
				$under_tickets_vis = explode(" (", $tot[$i][2]);
				$under_tickets_loc = explode(" (", $tot[$i+1][2]);
				//Asignamos los datos publics a partidos		
				$partidos[$cont]['ml_visitante'] = $mlp[$i][1];
				$partidos[$cont]['ml_local'] = $mlp[$i+1][1];
				$partidos[$cont]['sp_visitante'] = $spp[$i][1];
				$partidos[$cont]['sp_local'] = $spp[$i+1][1];
				$partidos[$cont]['to_visitante_over'] = $over_vis;
				$partidos[$cont]['to_visitante_under'] = $top[$i][2];
				$partidos[$cont]['to_local_over'] = $over_loc;
				$partidos[$cont]['to_local_under'] = $top[$i+1][2];
				//Calculamos el porcentaje de apuestas a favor
				$total_ml = (int) $ml_tickets_vis[0] + (int) $ml_tickets_loc[0];
				$porc_ml_vis = $this->calcular_porcentaje($ml_tickets_vis[0], $total_ml);
				$porc_ml_loc = $this->calcular_porcentaje($ml_tickets_loc[0], $total_ml);
				$total_sp = (int) $sp_tickets_vis[0] + (int) $sp_tickets_loc[0];
				$porc_sp_vis = $this->calcular_porcentaje($sp_tickets_vis[0], $total_sp);
				$porc_sp_loc = $this->calcular_porcentaje($sp_tickets_loc[0], $total_sp);
				$total_to = (int) $over_vis_tickets + (int) $under_tickets_vis[0];
				$porc_to_over = $this->calcular_porcentaje($over_vis_tickets, $total_to);
				$porc_to_under = $this->calcular_porcentaje($under_tickets_vis[0], $total_to);
				//Asignamos los porcentajes de apuestas a favor a partidos
				$partidos[$cont]['ml_visitante_tickets'] = $porc_ml_vis;
				$partidos[$cont]['ml_local_tickets'] = $porc_ml_loc;
				$partidos[$cont]['sp_visitante_tickets'] = $porc_sp_vis;
				$partidos[$cont]['sp_local_tickets'] = $porc_sp_loc;
				$partidos[$cont]['to_visitante_over_tickets'] = $porc_to_over;
				$partidos[$cont]['to_visitante_under_tickets'] = $porc_to_under;
				$partidos[$cont]['to_local_over_tickets'] = $porc_to_over;
				$partidos[$cont]['to_local_under_tickets'] = $porc_to_under;
				//Avanzamos en el array de equipos
				$cont++;
				$i++;
			}
			return $partidos;
		}
		//Calcula el porcentaje dado el nº de apuestas a favor del local/visitante y el total de apuestas realizadas
		private function calcular_porcentaje($part, $total){
			return round(((int) $part / $total)*100)."%";
		}
		//Función para pintar las filas, $porc = porcentaje a favor, $meta = a partir de este porcentaje se colorea en rojo
		public function paint($porc, $meta, $equipo){
			if ((int) trim($porc,"%")>=$meta) {
				echo ("<td style='background-color: #FF8080'>".$equipo." (".$porc.")</td>");
			} else {
				echo ("<td>".$equipo." (".$porc.")</td>");
			}
		}
		//Función para pintar las filas de totals, $meta = a partir de este porcentaje se colorea en rojo
		public function paint_totals($over, $under, $meta){
			if ((int) trim($over,"%")>=$meta || (int) trim($under,"%")>=$meta) {
				echo ("<td style='background-color: #FF8080'>Over (".$over."), Under (".$under.")</td>");	
			} else {
				echo ("<td>Over (".$over."), Under (".$under.")</td>");
			}
		}
	}
?>