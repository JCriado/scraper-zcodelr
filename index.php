<?php
	require_once("ZCodeScraper.php");
	$scraper = new ZCodeScraper('http://zcodesystem.com/linereversals.php');
	$equipos = $scraper->get_equipos();
	$fechas = $scraper->get_fecha_partidos();
	$ml_publics = $scraper->get_ml_publics();
	$sp_publics = $scraper->get_sp_publics();
	$to_publics = $scraper->get_to_publics();
	$ml_tickets = $scraper->get_ml_tickets();
	$sp_tickets = $scraper->get_sp_tickets();
	$to_tickets = $scraper->get_to_tickets();
	$partidos = $scraper->crear_partidos($equipos, $fechas, $ml_publics, $sp_publics, $to_publics, $ml_tickets, $sp_tickets, $to_tickets);
?>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
		<table class="table">
			<thead>
				<tr>
					<td><strong>Hora (+6h España)</strong></td>
					<td><strong>Public</strong></td>
					<td><strong>Local</strong></td>
					<td><strong>Visitante</strong></td>
					<td><strong>Tickets</strong></td>
					<td><strong>Local</strong></td>
					<td><strong>Visitante</strong></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($partidos as $match) {?>
					<!-- Money Line -->
					<!-- Si el % a favor supera o iguala el 80%, lo coloremos de rojo -->
					<tr>
						<td><?php echo $match['Fecha']?></td>
						<td>Money Line</td>
						<?php echo $scraper->paint($match['ml_local'], 80, $match['Local']) ?>
						<?php echo $scraper->paint($match['ml_visitante'], 80, $match['Visitante']) ?>
						<td>Money Line</td>
						<?php echo $scraper->paint($match['ml_local_tickets'], 80, $match['Local']) ?>
						<?php echo $scraper->paint($match['ml_visitante_tickets'], 80, $match['Visitante']) ?>
					</tr>
					<!-- Spread -->
					<tr style="background-color: #E8E8E8">
						<td><?php echo $match['Fecha']?></td>
						<td>Spread Line</td>
						<?php echo $scraper->paint($match['sp_local'], 80, $match['Local']) ?>
						<?php echo $scraper->paint($match['sp_visitante'], 80, $match['Visitante']) ?>
						<td>Spread Line</td>
						<?php echo $scraper->paint($match['sp_local_tickets'], 80, $match['Local']) ?>
						<?php echo $scraper->paint($match['sp_visitante_tickets'], 80, $match['Visitante']) ?>
					</tr>
					<!-- Total -->
					<tr style="background-color: #D8D8D8">
						<td><?php echo $match['Fecha']?></td>
						<td>Over/Under</td>
						<?php echo $scraper->paint_totals($match['to_local_over'], $match['to_local_under'], 80) ?>
						<?php echo $scraper->paint_totals($match['to_visitante_over'], $match['to_visitante_under'], 80) ?>
						<td>Over/Under</td>
						<?php echo $scraper->paint_totals($match['to_local_over_tickets'], $match['to_local_under_tickets'], 80) ?>
						<?php echo $scraper->paint_totals($match['to_visitante_over_tickets'], $match['to_visitante_under_tickets'], 80) ?>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>